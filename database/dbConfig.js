const mongoose = require('mongoose');

const connection = async() => {
    
    try {
        await mongoose.connect(process.env.DB_URL_CONNECTION);

        console.log('Database Connected!');

    } catch (error) {
        console.log(error);
        throw new Error('Could not connect to the database');
    }
}

module.exports = { connection }