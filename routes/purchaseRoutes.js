const express = require('express');
const { readXml } = require('../controllers/purchasesController');

const router = express.Router();

router.post('/readxml', readXml);

module.exports = router