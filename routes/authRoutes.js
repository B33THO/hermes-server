const express = require('express');
const { login, renewSession } = require('../controllers/authController');
const { check } = require('express-validator');
const { verifyToken } = require('../middlewares/validateJWT');
const { validateFields } = require('../middlewares/validateFields');


const router = express.Router();

//Login endpoint. Need user and password by headers
router.post('/login', 
    [
        check('user', 'Ingrese un usuario').not().isEmpty(),
        check('password', 'Ingrese una contraseña').not().isEmpty(),
        validateFields
    ],
    login);

router.get('/renewsession', verifyToken, renewSession);

module.exports = router;