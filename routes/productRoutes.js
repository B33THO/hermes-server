const express = require('express');
const { check } = require('express-validator');
const { loadProductsFromExcel } = require('../controllers/loadProductsController');

const { 
    getUnitsData, 
    saveTrademark, 
    getTrademarkList, 
    deleteTrademarkById, 
    getSatCodesList, 
    saveProduct,
    getProductList,
    getProductByCodeDescModelForProvider,
    getProductByCodeDescModel,
    editProductById,
    getTrademarkDataByName
} = require('../controllers/productController');

const { validateFields } = require('../middlewares/validateFields');
const { verifyToken } = require('../middlewares/validateJWT');
const { validateProduct } = require('../validators/product_validator');

const router = express.Router();

// Product CRUD
router.post('/saveproduct', 
[
    verifyToken,
    validateProduct
],
saveProduct);

router.get('/getproducts', verifyToken, getProductList);

router.get('/findproductprovider', verifyToken, getProductByCodeDescModelForProvider);

router.get('/findproduct', verifyToken, getProductByCodeDescModel);

router.post('/updateproduct/:id', verifyToken, editProductById);

// Trademark CRUD
router.get('/trademarks/list', getTrademarkList);

router.get('/trademarks/gettrademarkbyname/:trademarkname', getTrademarkDataByName);

router.post('/trademark/save',
    [
        verifyToken,
        check('trademark').exists().not().isEmpty(),
        validateFields
    ], 
    saveTrademark);

router.delete('/trademark/delete/', verifyToken, deleteTrademarkById);

// SAT Codes - read only
router.get('/satcode/list', verifyToken, getSatCodesList);

router.get('/measuringunits/list', verifyToken, getUnitsData);

//product loader
router.post('/loadproductsfromexcel', loadProductsFromExcel );

module.exports = router;