const express = require('express');
const { check } = require('express-validator');

const { saveNewProvider, updateProvider, getProviders, getProviderById } = require('../controllers/providerController');
const { validateEmail } = require('../middlewares/validateEmail');
const { validateFields } = require('../middlewares/validateFields');
const { verifyToken } = require('../middlewares/validateJWT');
const { validateProvider } = require('../validators/provider_validator');

const router = express.Router();

//Routes------
router.get('/provider/getproviders', verifyToken, getProviders);

router.post('/provider/new',
    [        
        verifyToken,
        validateProvider
    ], 
    saveNewProvider
);

router.get('/provider/getprovider/:id', verifyToken, getProviderById);

router.put('/provider/updateprovider/:id',
    [
        check('id', 'El id del provedor es requerido').not().isEmpty(),
        verifyToken,
        validateProvider
    ],
    updateProvider
);

module.exports = router;