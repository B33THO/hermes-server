const {Schema, model} = require('mongoose');

const OrderSchema = new Schema({

    order_num: {
        type: String,
        unique: true
    }
});

module.exports = model('Order', OrderSchema);