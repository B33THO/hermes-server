const { Schema, model } = require('mongoose');

const TrademarkSchema = Schema({
    trademark: {
        type: String,
        unique: true
    }
});

module.exports = model('Trademark', TrademarkSchema);