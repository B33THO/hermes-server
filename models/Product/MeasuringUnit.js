const { Schema, model} = require('mongoose');

const MeasuringUnitSchema = Schema({
    id: String,
    nombre: String,
    descripcion: String,
    nota: String,
    fechaDeInicioDeVigencia: Date,
    fechaDeFinDeVigencia: Date,
    simbolo: String
});

module.exports = model('MeasuringUnit', MeasuringUnitSchema);
