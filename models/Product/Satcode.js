const { Schema, model } = require('mongoose');

const SatcodeSchema = Schema({
    id: String,
    descripcion: String,
    incluirIVATrasladado: String,
    incluirIEPSTrasladado: String,
    complementoQueDebeIncluir: String,
    fechaInicioVigencia: Date,
    fechaFinVigencia: Date,
    estimuloFranjaFronteriza: Number,
    palabrasSimilares: String
});

module.exports = model('Satcode', SatcodeSchema);