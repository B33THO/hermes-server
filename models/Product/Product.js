
const { Schema, model } = require('mongoose');

const moment = require('moment');
moment.locale('es');

const ProductSchema = Schema({

    code: {
        type: String,
        required: true,
        unique: true
    },
    model: String,
    cost: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        trim: true,
        required: true
    },
    ean: String,
    location: String,    
    box: {
        type: Number,
        default: 0
    },
    master: {
        type: Number,
        default: 0
    },
    minimum_purchase: {
        type: Number,
        default: 1
    },
    quantitySold: {
        type: Number,
        default: 0
    },
    minimum: {
        type: Number,
        default: 1
    },
    maximum: {
        type: Number,
        default: 2
    },
    createAt: Date,
    active: {
        type: Boolean,
        default: true
    },
    utilities: {
        PPF: {
            min_ppf: Number,
            percentage_ppf: Number,
            desc: String
        },
        PPB: {
            min_ppb: Number,
            percentage_ppb: Number,
            desc: String
        },
        PMMY: {
            min_pmmy: Number,
            percentage_pmmy: Number,
            desc: String
        },
        PMY: {
            min_pmy: Number,
            percentage_pmy: Number,
            desc: String
        },
        PIND: {
            min_pind: Number,
            percentage_pind: Number,
            desc: String
        },
        PDST: {
            min_pdst: Number,
            percentage_pdst: Number,
            desc: String
        },
    },
    costHistory: [{
        cost: Number,
        date: {
            type: Date,
            default: Date.now()
        }
    }],
    provider: {
        type: Schema.Types.ObjectId,
        ref: 'Provider',
        required: true
    },
    trademark: {
        type: Schema.Types.ObjectId,
        ref: 'Trademark'
    },
    satcode: {
        type: Schema.Types.ObjectId,
        ref: 'Satcode'
    },
    measuringunit: {
        type: Schema.Types.ObjectId,
        ref: 'MeasuringUnit'
    }    

});

module.exports = model('Product', ProductSchema);