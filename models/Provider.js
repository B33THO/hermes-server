const { Schema, model } = require('mongoose');

const ProviderSchema = Schema({
    provider_name: {
        type: String,
        required: true,
        unique: true
    },
    providerAddress: {
        street: String,
        num_ext: String,
        num_int: String,
        suburb: String,
        location: String,
        reference: String,
        municipality: String,
        state: String,
        country: String,
        postalcode: Number
    },
    rfc: {
        type: String,
        required: true,
        unique: true
    },    
    credit_days: {
        type: Number,
        default: 0,
        required: true
    },
    credit_limit: {
        type: Number,
        default: 0
    },
    conditions: [String],

    phonenumbers: [{
        contact_phone: String,
        stall_phone: String,
        phonenumber: Number,
        extension: Number
    }],
    
    emails: [{
        contact_email: String,
        stall_email: String,
        email_address: String
    }],
    active: {
        type: Boolean,
        default: true
    },
    banksData: [{
        bank_name: String,
        CLABE: String,
        number_account: {
            type: String,
            trim: true,            
        },
        payment_reference: String
    }]
});


module.exports = model('Provider', ProviderSchema);