const XLSX = require('xlsx');

const readExcelFile = (path) => {
    
    const workbook = XLSX.readFile(path);
    const sheet = workbook.Sheets[ workbook.SheetNames[0] ];
    const data = XLSX.utils.sheet_to_json(sheet);

    //data.map( p => ( console.log(JSON.stringify(p))))

    return data;

}

module.exports = {
    readExcelFile
}