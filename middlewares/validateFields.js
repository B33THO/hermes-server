const express = require('express');
const { validationResult } = require('express-validator');


//función que revisa el objeto de errores que genera express validator despues de
//ejecutar los checks en las respectivas rutas.

const validateFields = (req, res= express.response, next) => {
    
    const errors = validationResult( req );

    if(!errors.isEmpty()) {
        
        console.log(errors);
        
        return res.status(400).json({
            ok: false,
            source: 'VALIDATOR',
            errors: errors.mapped()
        });
    }

    next();
}

module.exports = {
    validateFields
}