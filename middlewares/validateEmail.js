const { response } = require('express');
const { email_regex } = require('../regex');

const validateEmail = (req, res = response, next) => {
    
    
    let mails = [];
    req.body.emails.map(e => {
        mails.push(e);
    });

    for (let index = 0; index < mails.length; index++) {

        let email = mails[index].email_address.toString();

        if( email_regex.test(email) ){

        } else {
            console.log('Email Incorrecto');
            return res.status(500).json({
                ok: false,
                msg: `El correo ${email} no es válido`
            });
        }
    }

    next();
}

module.exports = {
    validateEmail
}