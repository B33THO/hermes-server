const { response } = require('express');
const jwt = require('jsonwebtoken');

// los middlewares son  funciones que nos ayudan a hacer validaciones
// en los parámetros reciben: request y response, y next para finalizar la comprobación

const verifyToken = (req, res = response, next) => {

    const token = req.header('x-token');

    if(!token){
        return res.status(401).json({
            ok: false,
            msg: 'no se encontro token'
        });
    }

    try {

        // se extrae el usuario del payload del token de la petición para verificarlo
        // Se logra la extracción únicamente si el token es válido, de lo contrario,
        // se lanza un error que es capturado en el catch.
        const {user}  = jwt.verify(token, process.env.SECRET_WORD);

        // Modificamos lo que hay en la request, con el usuario que extragimos 
        req.user = user;
    
        //finalización de la verificación
        next();
        
    } catch (error) {
        console.log(error);
        return res.status(401).json({
            ok: false,
            msg: 'token invalido o expirado'
        });
    }

}

module.exports = {verifyToken}