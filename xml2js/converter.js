const xml2js = require('xml2js');

const convert = (data) => {
    
    let parser = new xml2js.Parser({explicitArray: false});

    parser.parseString(data, (err, result) => {
        console.log('\n');
        console.log(JSON.stringify(result));
        return result;
    });
}

module.exports = {
    convert
}