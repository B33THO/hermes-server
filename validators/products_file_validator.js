const Satcode = require("../models/Product/Satcode");
const Trademark = require("../models/Product/Trademark");
const { isNumericValue } = require("../regex");


const productDataFileValidatorOK = (fileData, ok, errors) => {
    
    let index = 2;

    fileData.map( productRow => {

        if(!isCodeOk(productRow.code)){
            errors.push({
                index,
                field: 'code',
                failedValue: productRow.code
            });
            ok = false;
        }

        if(!isCostOK(productRow.cost)){
            errors.push({
                index,
                field: 'cost',
                failedValue: productRow.cost
            });
            ok = false;
        }

        if(isEmpty(productRow.description)){
            errors.push({
                index,
                field: 'description',
                failedValue: productRow.description
            });
            ok = false;
        }

        if(!isBoxOK(productRow.box)){
            errors.push({
                index,
                field: 'box',
                failedValue: productRow.box
            });
            ok = false;
        }

        if(!isMasterOK(productRow.master)){
            errors.push({
                index,
                field: 'master',
                failedValue: productRow.master
            });
            ok = false;
        }

        if(!isMinimunPurchaseOK(productRow.minimum_purchase)){
            errors.push({
                index,
                field: 'minimum_purchase',
                failedValue: productRow.minimum_purchase
            });
            ok = false;
        }

        if(!isMinimumOK(productRow.minimum)){
            errors.push({
                index,
                field: 'minimum',
                failedValue: productRow.minimum
            });
            ok = false;
        }

        if(!isMaximumOK(productRow.maximum)){
            errors.push({
                index,
                field: 'maximum',
                failedValue: productRow.maximum
            });
            ok = false;
        }

        if(!isNumericRequiredOK(productRow.min_ppf)){
            errors.push({
                index,
                field: 'min_ppf',
                failedValue: productRow.min_ppf
            });
            ok = false;
        }

        if(!isNumericRequiredOK(productRow.percentage_ppf)){
            errors.push({
                index,
                field: 'percentage_ppf',
                failedValue: productRow.percentage_ppf
            });
            ok = false;
        }

        if(!isNumericRequiredOK(productRow.min_ppb)){
            errors.push({
                index,
                field: 'min_ppb',
                failedValue: productRow.min_ppb
            });
            ok = false;
        }

        if(!isNumericRequiredOK(productRow.percentage_ppb)){
            errors.push({
                index,
                field: 'percentage_ppb',
                failedValue: productRow.percentage_ppb
            });
            ok = false;
        }

        if(!isNumericRequiredOK(productRow.min_pmmy)){
            errors.push({
                index,
                field: 'min_pmmy',
                failedValue: productRow.min_pmmy
            });
            ok = false;
        }

        if(!isNumericRequiredOK(productRow.percentage_pmmy)){
            errors.push({
                index,
                field: 'percentage_pmmy',
                failedValue: productRow.percentage_pmmy
            });
            ok = false;
        }

        if(!isNumericRequiredOK(productRow.min_pmy)){
            errors.push({
                index,
                field: 'min_pmy',
                failedValue: productRow.min_pmy
            });
            ok = false;
        }

        if(!isNumericRequiredOK(productRow.percentage_pmy)){
            errors.push({
                index,
                field: 'percentage_pmy',
                failedValue: productRow.percentage_pmy
            });
            ok = false;
        }

        if(!isNumericRequiredOK(productRow.min_pind)){
            errors.push({
                index,
                field: 'min_pind',
                failedValue: productRow.min_pind
            });
            ok = false;
        }

        if(!isNumericRequiredOK(productRow.percentage_pind)){
            errors.push({
                index,
                field: 'percentage_pind',
                failedValue: productRow.percentage_pind
            });
            ok = false;
        }

        if(!isNumericRequiredOK(productRow.min_pdst)){
            errors.push({
                index,
                field: 'min_pdst',
                failedValue: productRow.min_pdst
            });
            ok = false;
        }

        if(!isNumericRequiredOK(productRow.percentage_pdst)){
            errors.push({
                index,
                field: 'percentage_pdst',
                failedValue: productRow.percentage_pdst
            });
            ok = false;
        }
        index++;

    });
}


const isEmpty = (field) => (field === "" || field === undefined || field === null);
const isCodeOk = (code) => ( !isEmpty(code) );
const isCostOK = (cost) => ( isNumericValue(cost) && cost > 0 );
const isBoxOK = (box) => ( isNumericValue(box) || isEmpty(box));
const isMasterOK = (master) => ( isNumericValue(master) || isEmpty(master));
const isMinimunPurchaseOK = (minimunPurchase) => ( isNumericValue(minimunPurchase) || isEmpty(minimunPurchase));
const isMinimumOK = (minimum) => ( isNumericValue(minimum) || isEmpty(minimum));
const isMaximumOK = (maximum) => ( isNumericValue(maximum) || isEmpty(maximum));

const isNumericRequiredOK = ( value ) => ( isNumericValue(value) && !isEmpty(value) && value >= 0);


const getDiferentSatcodesValues = (arrsorc) => {
    let arr = [];
    arrsorc.map( item => {
        arr.push(item.satcode.toString());
    });

    let difItem = arr.reduce( (acc, item) => {
        if(!acc.includes(item)){
            acc.push(item);
        }
        return acc;
    }, []);
    return difItem;
}

const getDiferentSatUnits = (arrSrc) => {
    let arr = [];
    arrSrc.map( item => {
        arr.push(item.measuringunit);
    });

    let difItem = arr.reduce( (acc, item) => {
        if(!acc.includes(item)){
            acc.push(item);
        }
        return acc;
    }, []);
    return difItem;
}

const getDiferentTrademarks = (arrSrc) => {
    let arr = [];
    arrSrc.map( item => {
        arr.push(item.trademark);
    });

    let difItem = arr.reduce( (acc, item) => {
        if(!acc.includes(item)){
            acc.push(item);
        }
        return acc;
    }, []);
    return difItem;
}

module.exports = {
    productDataFileValidatorOK,
    getDiferentSatcodesValues,
    getDiferentSatUnits,
    getDiferentTrademarks 
}