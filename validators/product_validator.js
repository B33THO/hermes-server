const { check } = require('express-validator');
const { validateFields } = require('../middlewares/validateFields');
const { isNumericValue } = require('../regex');

const validateProduct = [

    check('code').exists().notEmpty().withMessage('El código es obligatorio'),

    check('cost').exists().notEmpty().withMessage('El costo es obligatorio').isNumeric(),

    check('description').exists().notEmpty().withMessage('La descripción del producto es obligatoria'),

    check('utilities').exists().notEmpty().custom( (value) => {

        const { PPF, PPB, PMMY, PMY, PIND, PDST } = value;

        //---------| PPF |-----------------------------------------------------------------------------------------------
        if( !isNumericValue(PPF.min_ppf) || Number(PPF.min_ppf) < 0) {            
            throw new Error('El valor del mínimo en PPF es incorrecto. Ingrese un valor numérico mayor o igual a 0');
        }

        if( !isNumericValue(PPF.percentage_ppf) || Number(PPF.percentage_ppf) < 0){
            throw new Error('El valor del porcentaje en PPF es incorrecto. Ingrese un valor numérico mayor o igual a 0');
        }

        //---------| PPB |-----------------------------------------------------------------------------------------------
        if( !isNumericValue(PPB.min_ppb) || Number(PPB.min_ppb) < 0) {            
            throw new Error('El valor del mínimo en PPB es incorrecto. Ingrese un valor numérico mayor o igual a 0');
        }

        if( !isNumericValue(PPB.percentage_ppb) || Number(PPB.percentage_ppb) < 0){
            throw new Error('El valor del porcentaje en PPB es incorrecto. Ingrese un valor numérico mayor o igual a 0');
        }

        //---------| PMMY |-----------------------------------------------------------------------------------------------
        if( !isNumericValue(PMMY.min_pmmy) || Number(PMMY.min_pmmy) < 0) {            
            throw new Error('El valor del mínimo en PMMY es incorrecto. Ingrese un valor numérico mayor o igual a 0');
        }

        if( !isNumericValue(PMMY.percentage_pmmy) || Number(PMMY.percentage_pmmy) < 0){
            throw new Error('El valor del porcentaje en PMMY es incorrecto. Ingrese un valor numérico mayor o igual a 0');
        }

        //---------| PMY |-----------------------------------------------------------------------------------------------
        if( !isNumericValue(PMY.min_pmy) || Number(PMY.min_pmy) < 0) {            
            throw new Error('El valor del mínimo en PMY es incorrecto. Ingrese un valor numérico mayor o igual a 0');
        }

        if( !isNumericValue(PMY.percentage_pmy) || Number(PMY.percentage_pmy) < 0){
            throw new Error('El valor del porcentaje en PMY es incorrecto. Ingrese un valor numérico mayor o igual a 0');
        }

        //---------| PIND |-----------------------------------------------------------------------------------------------
        if( !isNumericValue(PIND.min_pind) || Number(PIND.min_pind) < 0) {            
            throw new Error('El valor del mínimo en PIND es incorrecto. Ingrese un valor numérico mayor o igual a 0');
        }

        if( !isNumericValue(PIND.percentage_pind) || Number(PIND.percentage_pind) < 0){
            throw new Error('El valor del porcentaje en PIND es incorrecto. Ingrese un valor numérico mayor o igual a 0');
        }

        //---------| PDST |-----------------------------------------------------------------------------------------------
        if( !isNumericValue(PDST.min_pdst) || Number(PDST.min_pdst) < 0) {            
            throw new Error('El valor del mínimo en PDST es incorrecto. Ingrese un valor numérico mayor o igual a 0');
        }

        if( !isNumericValue(PDST.percentage_pdst) || Number(PDST.percentage_pdst) < 0){
            throw new Error('El valor del porcentaje en PDST es incorrecto. Ingrese un valor numérico mayor o igual a 0');
        }

        return true;
    }),

    (req, res, next) => {
        validateFields(req, res, next);
    }
];

module.exports = {
    validateProduct
}