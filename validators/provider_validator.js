const { check } = require('express-validator');
const { validateFields } = require('../middlewares/validateFields');
const {email_regex, onlyDigits_regex, isClabe, isNumericValue}  = require('../regex');

const validateProvider =  [

    check('provider_name')
        .exists().withMessage('No se recibio el campo nombre de proveedor')
        .not()
        .isEmpty().withMessage('Por favor, ingrese un nombre de proveedor')
        .isLength({ min: 5 }).withMessage('El nombre debe contener al menos 5 caracteres')
        .isString().withMessage(''),

    check('rfc')
        .exists()
        .isLength({min:13, max:13}),

    check('providerAddress')
        .custom( ( value ) => {
            
            const cp = value.postalcode;
            
            if( /^[d]{5}/g.test(cp) || cp < 0){
                throw new Error('Ingrese un codigo porstal válido');
            }

            return true;
        }),

    check('credit_limit')
        .isNumeric().withMessage('Ingrese un valor numérico')
        .custom( (value, {req}) => {
            
            if( !onlyDigits_regex.test(value) || Number(value) < 0 ){
                
                throw new Error('Ingrese un valor numérico positivo ó 0');
            }
            onlyDigits_regex.lastIndex = 0;
            return true;
        }),
    
    check('credit_days')
        .isNumeric().withMessage('Ingrese un valor numérico')    
        .custom( (value) => {
            
            console.log(`valor de credit days ${value}`);
            if( !onlyDigits_regex.test(value) || Number(value) < 0 ){
                throw new Error('Ingrese un valor numérico positivo ó 0');
            }
            onlyDigits_regex.lastIndex = 0;
            return true;
        }),
    
    check('emails')
        .custom( (value, {req}) => {
                        
            const { emails } = req.body;
    
            let mails = [];
            emails.map(e => {
                mails.push(e.email_address.toString());
            });

            mails.forEach(email => {
                if(!email_regex.test(email)){
                    throw new Error(`El correo ${email} no es válido`)
                }
            });
            return true;
        }),

    check('phonenumbers')
        .custom( (value, { req }) => {

            const { phonenumbers } = req.body;              
            let phones = [];

            phonenumbers.map( p => phones.push(p.phonenumber.toString()));

            phones.forEach(p => {
                
                if(p.length !== 10){
                    throw new Error('el teléfono debe ser de 10 dígitos');
                }
            });
            return true;
        }),

    check('banksData')        
        .custom( (value, { req }) => {
            
            const clabes = [];
            const numbers_account = [];

            value.map( clabe => {
                clabes.push(clabe.CLABE);
            })
            value.map( num_acc => {
                numbers_account.push(num_acc.number_account)
            })
            clabes.forEach(c => {
                if(!isClabe(c) || c.length > 18){
                    throw new Error('La CLABE introducida no tiene el formato correcto: Ser requieren 18 dígitos numéricos');
                }
            });          

            numbers_account.forEach( na => {
                if(!isNumericValue(na)){
                    throw new Error('El número de cuenta solo debe contener dígitos');
                }
            })
                        
            return true;
        }),
        
        (req, res, next) => {
            validateFields(req, res, next);
        }
]


module.exports = {validateProvider}