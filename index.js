const express = require('express');
require('dotenv').config();
const cors = require('cors');
const { connection } = require('./database/dbConfig');
const { readExcelFile } = require('./fileprocessor/xlsx');

const app = express();
connection();

app.use(cors());
app.use( express.static(__dirname+'public'));
app.use( express.json());
app.use('/hermesapi', require('./routes/authRoutes'));
app.use('/hermesapi', require('./routes/providerRoutes'));
app.use('/hermesapi/product', require('./routes/productRoutes'));
app.use('/hermesapi/purchase', require('./routes/purchaseRoutes'));

app.listen( process.env.PORT, () => {
    console.log(`Server online on port ${process.env.PORT}`);
});


