
const email_regex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
const thereAreLetters_regex = /\D/g;
const onlyDigits_regex = /^\d+$/g;
const numericValue_regex = /^[0-9]\d*(\.\d+)?$/g;

const isNumericValue = (value) => {
    
    //numericValue_regex.lastIndex = 0;
    const isNumeric = new RegExp(/^[0-9]\d*(\.\d+)?$/, 'i');
    return isNumeric.test(value);

}

const isClabe = value => {

    const isClabe = new RegExp(/^\d{18}$/, 'i');
    return isClabe.test(value);
}



module.exports = {
    email_regex,
    thereAreLetters_regex,
    onlyDigits_regex,
    numericValue_regex,
    isNumericValue,
    isClabe
}