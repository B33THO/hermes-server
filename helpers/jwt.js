const jwt = require ('jsonwebtoken');

const genJWT = (user) => {
    
    return new Promise( (resolve, reject) => {

        const payload = {user};

        jwt.sign(payload, process.env.SECRET_WORD, {expiresIn: '3h'}, (err, token)=>{

            if(err){
                console.log(err);
                reject('No se pudo generar el token');
            }

            resolve( token );
        });

    });
}

module.exports = { genJWT }