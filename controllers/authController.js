const { response } = require('express');
const { genJWT } = require('../helpers/jwt');


const login = async(req, res = response) => {

    const {user, password } = req.body;
  

    if(process.env.USER === user && process.env.PASSWORD === password) {

        const token = await genJWT(user);
        
        return res.status(200).json({
            ok: true,
            token
        });

    } else {
        return res.status(401).json({
            ok: false,
            msg: 'Usuario o contraseña incorrecta',

        });
    }

}

//TODO: revalidar token
const renewSession = async(req, res = response) => {

    const user = req.user
    const newToken = await genJWT(user);

    return res.status(200).json({
        ok: true,
        user,
        newToken
    });
}

module.exports = {
    login,
    renewSession
}