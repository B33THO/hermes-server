const express = require('express');
const MeasuringUnit = require('../models/Product/MeasuringUnit');
const Trademark = require('../models/Product/Trademark');
const Satcode = require('../models/Product/Satcode');
const Product = require('../models/Product/Product');
const moment = require('moment');


const saveProduct = async(req, res = express.response) => {
    
    try {
        const product = new Product(req.body);
        await product.save();
        
        res.status(200).json({
            ok: true,
            msg: 'Producto Guardado',
            product
        });

    } catch (error) {
        console.log(error);
        
        if(error.code ==11000){
            return res.status(400).json({
                ok:false,
                source: 'DB',
                msg: 'Un producto ya esta registrado con ese código',
                errors: error
            });
        }
    }
}

const getProductList = async(req, res = express.response) => {
    
    const { page,} = req.query;

    try {
        const countProducts = await Product.countDocuments();
        const products = await Product.find();
        console.log(products);

        res.status(200).json({
            ok:true,
            products
        });

    } catch (error) {
        console.log(error);
    }
}

const getProductByCodeDescModelForProvider = async(req, res = express.response) => {
    
    const {find, page, providerId} = req.query;
    const limit = 10;   
    try {
        const countProducts = await Product.where({
            provider: providerId,
            $or: [
                {code: new RegExp('.*' + find + '.*', 'i')},
                {description: new RegExp('.*' + find + '.*', 'i')},
                {model: new RegExp('.*' + find + '.*', 'i')},
                {ean: new RegExp('^' + find + '$', 'i')}
            ]
        }).countDocuments();
        
        const product = await Product.find({
            provider: providerId,
            $or: [
                {code: new RegExp('.*' + find + '.*', 'i')},
                {description: new RegExp('.*' + find + '.*', 'i')},
                {model: new RegExp('.*' + find + '.*', 'i')},
                {ean: new RegExp('^' + find + '$', 'i')}
            ]
        }).skip( (page-1)*limit ).limit(limit);

    
        res.status(200).json({
            ok:true,
            product,
            totalPages: Math.ceil(countProducts/limit),
            currentPage: page,
            results: countProducts
        });
    } catch (error) {
        console.log(error);
        res.status(400).json({
            ok: false,
            error
        });
    }
}

const getProductByCodeDescModel = async(req, res = express.response) => {

    const { find, page } = req.query;
    const limit = 15;

    try {
    
        const countProducts = await Product.where({
            $or: [
                { code: new RegExp('.*' +find+ '.*', 'i') },
                { description: new RegExp('.*' +find+ '.*', 'i') },
                { model: new RegExp('.*' +find+ '.*', 'i') },
                { ean: new RegExp('^' +find+ '$', 'i') }
            ]
        }).countDocuments();
    
        const products = await Product.aggregate([
            {
                $match: {
                    
                    $or: [
                        { code: new RegExp('.*' +find+ '.*', 'i') },
                        { description: new RegExp('.*' +find+ '.*', 'i') },
                        { model: new RegExp('.*' +find+ '.*', 'i') },
                        { ean: new RegExp('^' +find+ '$', 'i') }
                    ]                    
                }
            },

            {
                $lookup: {
                  from: 'providers',
                  localField: 'provider',
                  foreignField: '_id',
                  as: 'providerData'
                }
              },
            
              { 
                $unwind: {
                  path: '$providerData',
                  includeArrayIndex: '0',
                  preserveNullAndEmptyArrays: false
                }
              },
            
              {
                $lookup: {
                  from: 'trademarks',
                  localField: 'trademark',
                  foreignField: '_id',
                  as: 'trademarkData'
                }
              },
            
              
              {
                $unwind: {
                  path: "$trademarkData",
                  includeArrayIndex: '0',
                  preserveNullAndEmptyArrays: false
                }
              },
              
              {
                $lookup: {
                  from: 'measuringunits',
                  localField: 'measuringunit',
                  foreignField: '_id',
                  as: 'measuringunitData'
                }
              },
            
              {
                $unwind: {
                  path: "$measuringunitData",
                  preserveNullAndEmptyArrays: false
                }
              },
            
              {
                $lookup: {
                  from: 'satcodes',
                  localField: 'satcode',
                  foreignField: '_id',
                  as: 'satcodeData'
                }
              },
            
              {
                $unwind: {
                  path: '$satcodeData',
                  preserveNullAndEmptyArrays: false
                }
              }
              

        ]).skip( (page-1) * limit ).limit( limit );
    
        res.status(200).json({
            ok:true,
            products,
            totalPages: Math.ceil(countProducts/limit),
            currentPage: page,
            results: countProducts
        });

    } catch (error) {
        console.log(error);
        res.status(400).json({
            ok: false,
            error
        });
    }


}

const editProductById = async( req, res = express.response) => {
    
    const idProduct = req.params.id;
    const productToUpdate = req.body;    

    try {

        const {cost} = await Product.findById( idProduct, {cost: 1} );    
        const productUpdated = await Product.findByIdAndUpdate(idProduct, productToUpdate, {new: true});

        
        if(Number(cost) !== Number(productToUpdate.cost)){


            moment.locale();
            const date = moment().format('MM DD YYYY h:mm:ss a');
            const newCost = productToUpdate.cost;           
            console.log(newCost);
            const p = await Product.findByIdAndUpdate(idProduct, {
                $addToSet: {
                    costHistory: {cost: newCost, date}
                }
            }, {new:true});
            console.log(p);

        }   

        res.status(200).json({
            ok: true,
            msg: 'Producto Actualizado',
            productUpdated
        });
    } catch (error) {
        console.log(error);
    }
}


//Trademark controllers
const saveTrademark = async(req, res = express.response) => {
    
    try {
        const newTrademark = new Trademark(req.body);
        await newTrademark.save();

        res.status(200).json({
            ok: true,
            msg: 'Marca Guardada',
            newTrademark
        });
        
    } catch (error) {
        console.log(error);
        if(error.code === 11000){

            return res.status(400).json({
                ok: false,
                source: 'DB',
                msg: 'La Marca ya esta registrada',
                errors: error
            });
        }
    }
}

const getTrademarkList = async(req, res = express.response) => {
    
    try {

        const trademarklist = await Trademark.find();
        
        res.status(200).json({
            ok: true,
            trademarklist
        });

    } catch (error) {
        console.log(error);
    }
}

const getTrademarkDataByName = async(req, res = express.response) => {
    
    const {trademarkname} = req.params;

    try {
        
        const trademarkData = await Trademark.find({trademark: trademarkname});

        res.status(200).json({
            ok: true,
            trademarkData
        });

    } catch (error) {
        console.log(error);
    }
}

const deleteTrademarkById = async(req, res = express.response) => {

    try {

        const id = req.body.id;
        await Trademark.findByIdAndDelete(id);
        
        res.status(200).json({
            ok: true,
            msg: 'Marca eliminada'
        });
    } catch (error) {
        console.log(error);
        res.status(401).json({
            ok: false,
            source: 'DB',
            error
        });
    }
}


//#region SAT codes controllers
const getSatCodesList = async(req , res = express.response) => {
    
    const { page, find  } = req.query;
    const limit = 5;

    try {
         
        const countDocs = await Satcode.where({
            $or: [
                {id: new RegExp('.*'+find+'.*', "i")},
                {descripcion: new RegExp('.*'+find+'.*', "i")}
            ]
        }).countDocuments();

        const satcodes = await Satcode.find({
            $or: [
                {id: new RegExp('.*'+find+'.*', "i")},
                {descripcion: new RegExp('.*'+find+'.*', "i")}
            ]
            
        }).skip( (page - 1) * limit).limit(limit * 1);


        res.status(200).json({
            ok:true,
            satcodes,
            totalPages: Math.ceil(countDocs/limit),
            currentPage: page
        });

    } catch (error) {
        console.log(error);
        res.status(400).json({
            ok: false,
            source: 'DB',
            error
        });
    }
}
//#endregion

const getUnitsData = async(req, res = express.response) => {
    
    const limit = 5
    const {page, find} = req.query;

    try {

        console.log(page, find);

        const countDocs = await MeasuringUnit.where({
            $or: [
                {id: new RegExp('.*'+find+'.*', 'i')},
                {nombre: new RegExp('.*'+find+'.*', 'i')}
            ]
        }).countDocuments();

        
        const units = await MeasuringUnit.find({
            $or: [
                {id: new RegExp('.*'+find+'.*', 'i')},
                {nombre: new RegExp('.*'+find+'.*', 'i')}
            ]
        }).skip((page - 1) * limit).limit(limit *1);

        console.log(units);
        return res.status(200).json({
            ok: true,
            units,
            totalPages: Math.ceil(countDocs/limit),
            currentPage: page
        });
    } catch (error) {
        console.log(error);
        res.status(400).json({
            ok: false,
            source: 'DB',
            error
        });
    }
}
module.exports = {
    saveProduct,
    getUnitsData,
    saveTrademark,
    getTrademarkList,
    getTrademarkDataByName,
    deleteTrademarkById,
    getSatCodesList,
    getProductList,
    getProductByCodeDescModelForProvider,
    getProductByCodeDescModel,
    editProductById
}