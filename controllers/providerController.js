const express = require('express');
const Provider = require('../models/Provider');

const saveNewProvider = async(req, res = express.response) => {

    const provider = new Provider(req.body);
    
    console.log(provider);
    try {              
        
        await provider.save();

        return res.status(200).json({
            ok: true,
            msg: 'Provedor guardado correctamente...'
        });

    } catch (error) { 

        console.log(error);

        if(error.code === 11000){
            
            return res.status(400).json({
                ok: false,            
                error,
                source: 'DB',
                msg: 'El RFC o NOMBRE ingresado ya existe en la base de datos'
            });
        }

        return res.status(400).json({
            ok: false,            
            errors: error,
            source: 'DB',
            msg: 'Ocurrio un error al intentar guardar el provedor'
        });
    }

}

const getProviderById = async( req, res = express.response) => {

    const idProvider = req.params.id
    console.log(idProvider);

    try {
        
        const providerFound = await Provider.findById(idProvider);
        return res.status(200).json({
            ok:true,
            providerFound
        });
    } catch (error) {
        console.log(error);
        return res.status(200).json({
            ok:false,
            error
        });
        
    }
}

const updateProvider = async(req, res = express.response) => {

    try {

        const id_provider = req.params.id;
        const provider = {
            ...req.body
        }

        const providerEdited = await Provider.findByIdAndUpdate(id_provider, provider, {new: true});        
    
        return res.status(200).json({
            ok: true,
            msg: 'Provedor actualizado correctamente...',
            providerEdited
        });

    } catch (error) {
        console.log(error);
    }
    
}

const getProviders = async( req, res = express.response) => {

    try {
        const providers = await Provider.find();
        return res.status(200).json({
            ok: true,
            providers
        })
    } catch (error) {
        console.log(error);
        return res.status(400).json({
            ok: false,
            msg: 'No se pudo cargar proveedores'
        })
    }
        
}

module.exports = {
    saveNewProvider,
    updateProvider,
    getProviders,
    getProviderById
}