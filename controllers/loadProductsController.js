const express = require('express');
const { readExcelFile } = require('../fileprocessor/xlsx');
const MeasuringUnit = require('../models/Product/MeasuringUnit');
const Product = require('../models/Product/Product');
const Satcode = require('../models/Product/Satcode');
const Trademark = require('../models/Product/Trademark');
const Provider = require('../models/Provider');
const { 
    productDataFileValidatorOK, 
    getDiferentSatcodesValues, 
    getDiferentSatUnits, 
    getDiferentTrademarks} = require('../validators/products_file_validator');

const loadProductsFromExcel = async(req, res = express.response) => {
    
    try {
        const path = req.body.path;
        const providerId = req.body.provider;

        const productsReaded = readExcelFile(path);
        let errors = [];
        let dbDataErrors = [];
        let ok = true;
        const satcodes = getDiferentSatcodesValues(productsReaded);
        const units = getDiferentSatUnits(productsReaded);
        const trademarks = getDiferentTrademarks(productsReaded);

        console.log(productsReaded);

        let trademarksFound = [];
        let satcodesFound = [];
        let unitsFound = [];
        
        for(let i = 0; i < trademarks.length; i++) {

            const tmExsist = await Trademark.findOne({
                trademark: { $regex: new RegExp("^"+trademarks[i]+"$", "i")}
            });

            console.log(tmExsist);

            if(tmExsist.length === 0) {
                dbDataErrors.push({
                    value: trademarks[i],
                    msg: 'Marca no registrada en la base de datos. Antes de cargar el archivo registre la marca'
                });
            } else {
                trademarksFound.push(tmExsist);
            }
        }

        for( let i = 0; i < satcodes.length; i++){
            
            const stExsist = await Satcode.findOne({ id: satcodes[i]});
            console.log(stExsist);
            if(stExsist.length === 0){
                
                dbDataErrors.push({
                    value: satcodes[i],
                    msg: 'El código no se encuentra en el catálogo del SAT.'
                })
                console.log(`El código ${satcodes[i]} es incorrecto `);
            } else {
                satcodesFound.push(stExsist);
            }            
        }

        for( let i = 0; i< units.length; i++){

            const unitExist = await MeasuringUnit.findOne( {nombre: {$regex: new RegExp("^"+units[i]+"$", "i") }});
            console.log(unitExist);
            if(unitExist.length === 0){

                dbDataErrors.push({
                    value: units[i],
                    msg: 'La unidad de medida no existe en el catálogo del SAT'
                })
            } else {
                unitsFound.push(unitExist);
            }
        }
        
        productDataFileValidatorOK(productsReaded, ok, errors);
        
        if(dbDataErrors.length !== 0){
            ok = false;
            return res.status(400).json({
                ok,
                errors: dbDataErrors
            });
            
        }

        if(errors.length !== 0){
            ok = false;
            return res.status(400).json({
                ok,
                errors: errors
            });
        }

        

        //File OK
        ok = true;
        let counter = 0;
        let products = [];

        // console.log(trademarksFound);
        // console.log(satcodesFound);
        // console.log(unitsFound);

        for(let i = 0; i < productsReaded.length; i++) {
            products.push( buildProduct(
                productsReaded[i], 
                providerId, 
                trademarksFound, 
                satcodesFound,
                unitsFound
            ));
            counter++;
        }

        await Product.insertMany(products);

        return res.status(200).json({
            ok,
            products_inserted: counter,
            msg: 'File loaded'
        })
        
        
        
    } catch (error) {
        console.log(error);
        const msg = error.code;
        if(msg === 11000){
            res.status(400).json({
                ok: false,
                msg: 'Error al intentar procesar el archivo. Uno o varios productos en el archivo, ya estan en la base de datos',
                error
            })
        }
    }
}


const buildProduct = (data, providerId, marks, satcodes, units) => {

    console.log(data);
    
    const product={
        code: data.code,
        model: data.model,
        cost: data.cost,
        description: data.description,
        ean: data.ean,
        location: data.location,
        box: data.box,
        master: data.master,
        minimum_purchase: data.minimum_purchase,
        quantitySold: 0,
        minimum: data.minimum,
        maximum: data.maximum,
        utilities: {
            PPF: {
                min_ppf: data.min_ppf,
                percentage_ppf: data.percentage_ppf
            },
            PPB: {
                min_ppb: data.min_ppb,
                percentage_ppb: data.percentage_ppb
            },
            PMMY: {
                min_pmmy: data.min_pmmy,
                percentage_pmmy: data.percentage_pmmy
            },
            PMY: {
                min_pmy: data.min_pmy,
                percentage_pmy: data.percentage_pmy
            },
            PIND: {
                min_pind: data.min_pind,
                percentage_pind: data.percentage_pind
            },
            PDST: {
                min_pdst: data.min_pdst,
                percentage_pdst: data.percentage_pdst
            }
        },
        costHistory: {cost: data.cost},
        provider: providerId,
        trademark: marks.filter( m => m.trademark === data.trademark)[0]._id,
        satcode: satcodes.filter( sc => sc.id === data.satcode.toString())[0]._id,
        measuringunit: units.filter( u => u.nombre.toUpperCase() === data.measuringunit.toUpperCase())[0]._id
    }
    return product;

}


module.exports = {
    loadProductsFromExcel
}